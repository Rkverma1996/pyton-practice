class StudentDetails:
  ''' this class is used only for to explain concept about instance variable in python '''
  def __init__(self, name, clas, rollno): # constructor
    self.name=name # instance variable it uses self keyword
    self.clas=clas
    self.rollno=rollno

  def rohit(self, teacherName, lastname): # instance method
    self.teacherName=teacherName # instance variable
    self.lastName=lastname
    print(self.teacherName+' '+self.lastName)

e=StudentDetails('rohiit','XII', '8624346')
print(e)  #  <__main__.StudentDetails object at 0x000002011C2E7850>
print(e.__dict__) # {'name': 'rohiit', 'clas': 'XII', 'rollno': '8624346'}

e.rohit('sanjeev', 'verma')
print(e.__dict__) # {'name': 'rohiit', 'clas': 'XII', 'rollno': '8624346', 'teacherName': 'sanjeev', 'lastName': 'verma'}

e.subject='biology' # instance variable by outer class
print(e.__dict__) # {'name': 'rohiit', 'clas': 'XII', 'rollno': '8624346', 'teacherName': 'sanjeev', 'lastName': 'verma', 'subject': 'biology'}

# how to access instance variable
print(e.name+'   '+ e.teacherName) # rohiit   sanjeev

# delete instance variable from outside
del e.teacherName
del e.clas # delete this variable from class
del e.name

print(e.__dict__) # {'rollno': '8624346', 'lastName': 'verma', 'subject': 'biology'}